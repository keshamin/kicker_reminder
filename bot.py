# -*- coding: utf-8 -*-
import telebot, sqlite3, os
import config
from tokens import TOKEN, TOKEN_DEV
import calendar_fetcher as cf
from calendar_fetcher import events_list_to_reply


bot = telebot.TeleBot(TOKEN) if 'mac_dev_flag' not in os.listdir() else telebot.TeleBot(TOKEN_DEV)
bot.send_message(config.ADMIN_ID, 'Wow, I\'ve just got (re)started.')


with sqlite3.connect('db.sqlite3') as conn:
    cur = conn.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS subs (
                   id int NOT NULL PRIMARY KEY,
                   subs text)''')
    cur.execute('''CREATE TABLE IF NOT EXISTS requests (
                   id int PRIMARY KEY,
                   lastname text,
                   msg_id int)''')
    cur.execute('''CREATE TABLE IF NOT EXISTS users (
                   id INT PRIMARY KEY,
                   lastname TEXT,
                   alarm TEXT)''')
    cur.execute('''CREATE TABLE IF NOT EXISTS events (
                   event_id INTEGER PRIMARY KEY,
                   timestamp int,
                   player0 text,
                   player1 text,
                   score text,
                   player2 text,
                   player3 text,
                   title text,
                   "date" text,
                   UNIQUE (title, "date"))''')
    cur.execute('''CREATE TABLE IF NOT EXISTS stats (
                   lastname text PRIMARY KEY,
                   games int,
                   wins int,
                   gd int,
                   points int)''')
    conn.commit()


def admin_only(bot):
    def decorator(func):
        def wrapper(message):
            if message.chat.id == config.ADMIN_ID:
                func(message)
            else:
                bot.send_message(message.chat.id, 'Вы должны быть администратором, чтобы использовать эту команду')
        return wrapper
    return decorator


@bot.message_handler(commands=['start', 'help'])
def start(message):
    bot.send_message(message.chat.id, 'Базовые команды:\n'
                                      '/today - Матчи на сегодня.\n'
                                      '/tomorrow - Матчи на завтра\n'
                                      '/upcoming - Все запланированные матчи\n'
                                      '<today|tomorrow|upcoming> [last name] - Кастомный запрос к календарю\n'
                                      '/players - Список игроков\n'
                                      '/help - Эта справка\n\n'
                                      'Подписка на рассылку в 11:00:\n'
                                      '/subs - Мои подписки\n'
                                      'subscribe [last name] - Подписаться на напоминания о '
                                      'предстоящих матчах игрока\n'
                                      'unsubscribe [last name] - Отменить подписку на игрока\n\n'
                                      'Персонализированные команды:\n'
                                      'register [last name] - Заявка на регистрацию '
                                      'для использования персонализированных команд\n'
                                      '/mytoday - Мои матчи на сегодня\n'
                                      '/mytomorrow - Мои матчи на завтра\n'
                                      '/myupcoming - Все мои будущие матчи')


@bot.message_handler(commands=['crash'])
@admin_only(bot)
def crash(message):
    bot.send_message(message.chat.id, 'Self-distructing...')
    raise KeyboardInterrupt


@bot.message_handler(commands=['upcoming'])
def send_future_events(message):
    reply = events_list_to_reply(cf.get_events('upcoming'))
    bot.send_message(message.chat.id, reply)


@bot.message_handler(commands=['today'])
def send_today_events(message):
    reply = events_list_to_reply(cf.get_events('today'))
    bot.send_message(message.chat.id, reply)


@bot.message_handler(commands=['tomorrow'])
def send_today_events(message):
    reply = events_list_to_reply(cf.get_events('tomorrow'))
    if reply != '':
        bot.send_message(message.chat.id, reply)
    else:
        bot.send_message(message.chat.id, 'На завтра не запланировано матчей.')


@bot.message_handler(commands=['players'])
def send_players_list(message):
    reply = 'Список игроков, найденных в Google календаре:\n'
    for player in cf.get_all_players():
        reply += player + '\n'
    if reply.count('\n') > 1:
        bot.send_message(message.chat.id, reply)
    else:
        bot.send_message(message.chat.id, 'В календаре не найдено ни одного игрока.\n'
                                          'Пожалуйста, свяжитесь с админом: {}'.format(config.ADMIN_USERNAME))


@bot.message_handler(commands=['subs'])
def send_subscriptions(message):
    conn = sqlite3.connect('db.sqlite3')
    cur = conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS subs (id int NOT NULL PRIMARY KEY, subs text)")
    subs = cur.execute("SELECT subs FROM subs WHERE id=?", (message.chat.id, )).fetchone()
    if subs is not None:
        subs = subs[0].strip().split(' ')
    else:
        subs = []
    if len(subs) > 0:
        reply = 'Ваши текущие подписки:\n'
        for lastname in subs:
            reply += lastname + '\n'
    else:
        reply = 'У вас не активировано ни одной подписки.'
    conn.commit()
    conn.close()
    bot.send_message(message.chat.id, reply)


@bot.message_handler(regexp='\\bsubscribe[\s]*[\w]+')
def subscribe(message):
    lastname = message.text.split()[1].lower()
    if cf.is_player(lastname):
        conn = sqlite3.connect('db.sqlite3')
        cur = conn.cursor()
        cur.execute("CREATE TABLE IF NOT EXISTS subs (id int NOT NULL PRIMARY KEY, subs text)")
        if cur.execute('SELECT * FROM subs WHERE id=?', (message.chat.id, )).fetchone() is None:
            cur.execute("INSERT INTO subs VALUES (?, ?)", (message.chat.id, lastname))
        else:
            subs = cur.execute("SELECT subs FROM subs WHERE id=?", (message.chat.id,)).fetchone()[0].split(' ')
            if lastname not in subs:
                cur.execute("""UPDATE subs SET subs=subs || ' ' || ? WHERE id=?""", (lastname, message.chat.id))
        conn.commit()
        conn.close()
        bot.send_message(message.chat.id, 'Подписка на игрока {} активирована.\n'
                                          'Я буду напоминать о предстоящих матчах в {}.'.format(lastname,
                                           config.REMIND_TIME.strftime('%H:%M')))
    else:
        bot.send_message(message.chat.id, 'Игрока нет в Google календаре :(\n'
                                          'Список всех игроков: /players')



@bot.message_handler(regexp='\\bunsubscribe[\s]*[\w]+')
def unsubscribe(message):
    lastname = message.text.split()[1].lower()
    if cf.is_player(lastname):
        conn = sqlite3.connect('db.sqlite3')
        cur = conn.cursor()
        cur.execute("CREATE TABLE IF NOT EXISTS subs (id int NOT NULL PRIMARY KEY, subs text)")
        sub = cur.execute('SELECT * FROM subs WHERE id=?', (message.chat.id,)).fetchone()
        if sub is not None:
            sub_set = set(sub[1].strip().split(' '))
            sub_set.discard(lastname)
            sub_str = ' '.join(sub_set)
            cur.execute("UPDATE subs SET subs=? WHERE id=?", (sub_str, message.chat.id))
        else:
            pass
        conn.commit()
        conn.close()
        bot.send_message(message.chat.id, 'Подписка на игрока {} отменена.\n'.format(lastname))
    else:
        bot.send_message(message.chat.id, 'Игрока нет в Google календаре :(\n'
                                          'Список всех игроков: /players')


@bot.message_handler(regexp='^(?i)register[\s]+[\w]+$')
def register(message):
    lastname = message.text.split()[1]
    if cf.is_player(lastname):  # player is in calendar?
        with sqlite3.connect('db.sqlite3') as conn:
            cur = conn.cursor()
            cur.execute('CREATE TABLE IF NOT EXISTS requests (id int PRIMARY KEY, lastname text, msg_id int)')
            # player is not registered?
            if cur.execute('SELECT * FROM users WHERE id=?', (message.chat.id, )).fetchone() is None:
                # player doesn't have a request in process?
                if cur.execute('SELECT * FROM requests WHERE id=?', (message.chat.id, )).fetchone() is None:
                    sent = bot.forward_message(config.ADMIN_ID, message.chat.id, message.message_id)
                    bot.send_message(config.ADMIN_ID, '{} {} {}'.format(message.chat.id, lastname, message.message_id))
                    cur.execute('INSERT INTO requests VALUES (?, ?, ?)',
                                (message.chat.id, lastname, sent.message_id))
                    conn.commit()
                    bot.send_message(message.chat.id, 'Ваша заявка обрабатывается. Я оповещу вас о подтверждении данных.')
                else:
                    bot.send_message(message.chat.id, 'Ваша заявка обрабатывается.')
            else:
                bot.send_message(message.chat.id, 'Вы уже зарегистрированы')
    else:
        bot.send_message(message.chat.id, 'Игрока нет в Google календаре :(\n'
                                          'Список всех игроков: /players')


@bot.message_handler(commands=['requests'])
@admin_only(bot)
def send_requests(message):
    with sqlite3.connect('db.sqlite3') as conn:
        cur = conn.cursor()
        cur.execute('CREATE TABLE IF NOT EXISTS requests (id int PRIMARY KEY, lastname text, msg_id int)')
        if cur.execute('SELECT * FROM requests').fetchone() is not None:
            for request in cur.execute('SELECT * FROM requests').fetchall():
                bot.send_message(message.chat.id, '{} {} {}'.format(request[0], request[1], request[2]))
        else:
            bot.send_message(message.chat.id, 'Нет необработанных заявок')


@bot.message_handler(regexp='^(?i)confirm$')
@admin_only(bot)
def confirm_registration(message):
    if message.reply_to_message is not None and len(message.reply_to_message.text.split()) == 3:    # the message is reply?
        with sqlite3.connect('db.sqlite3') as conn:
            cur = conn.cursor()
            user_id = message.reply_to_message.text.split()[0]
            # there's such request?
            cur.execute('CREATE TABLE IF NOT EXISTS requests (id int PRIMARY KEY, lastname text, msg_id int)')
            if cur.execute('SELECT * FROM requests WHERE id=?', (user_id, )).fetchone() is not None:
                # no such id in users?
                if cur.execute('SELECT * FROM users WHERE id=?', (user_id, )).fetchone() is None:
                    request = cur.execute('SELECT * FROM requests WHERE id=?', (user_id, )).fetchone()
                    cur.execute('CREATE TABLE IF NOT EXISTS users '
                                '(id INT PRIMARY KEY, lastname TEXT, alarm TEXT)')
                    cur.execute('INSERT INTO users VALUES (?, ?, ?)',
                                (request[0], request[1], '11:00'))
                    cur.execute('DELETE FROM requests WHERE id=?', (user_id, ))
                    bot.send_message(message.chat.id, 'Игрок {} зарегистрирован.'.format(request[1]))
                    conn.commit()

                    bot.send_message(request[0], 'Вы зарегистрированы. Теперь вам доступны персонализированные команды!')
                else:
                    bot.send_message(message.chat.id, 'Игрок уже зарегистрирован')
            else:
                bot.send_message(message.chat.id, 'Не существует такой заявки')
    else:
        bot.send_message(message.chat.id, 'Неверный ввод. Нужно ответить на сообщение вида:\n'
                                          '"<id> <lastname> <msg_id>"')


@bot.message_handler(regexp='^(?i)deny$')
@admin_only(bot)
def deny_registration(message):
    if message.reply_to_message is not None and len(message.reply_to_message.text.split()) == 3:    # the message is reply?
        with sqlite3.connect('db.sqlite3') as conn:
            cur = conn.cursor()
            user_id = message.reply_to_message.text.split()[0]
            cur.execute('CREATE TABLE IF NOT EXISTS requests (id int PRIMARY KEY, lastname text, msg_id int)')
            # there's such request?
            if cur.execute('SELECT * FROM requests WHERE id=?', (user_id, )).fetchone() is not None:
                request = cur.execute('SELECT * FROM requests WHERE id=?', (user_id,)).fetchone()
                cur.execute('DELETE FROM requests WHERE id=?', (user_id, ))
                conn.commit()
                bot.send_message(message.chat.id, 'ОК, заявка отклонена.')
                bot.send_message(request[0], 'Ваша заявка на регистрацию отклонена.')
            else:
                bot.send_message(message.chat.id, 'Не существует такой заявки')
    else:
        bot.send_message(message.chat.id, 'Неверный ввод. Нужно ответить на сообщение вида:\n'
                                          '"<id> <lastname> <msg_id>"')


@bot.message_handler(commands=['mytoday', 'mytomorrow', 'myupcoming'])
def send_personal_today_events(message):
    period = message.text.replace('/my', '').strip()
    with sqlite3.connect('db.sqlite3') as conn:
        cur = conn.cursor()
        user = cur.execute('SELECT * FROM users WHERE id=?', (message.chat.id, )).fetchone()
        if user is not None:
            reply = events_list_to_reply(cf.get_events(period, user[1]))
            bot.send_message(message.chat.id, reply)
        elif cur.execute('SELECT * FROM requests WHERE id=?', (message.chat.id, )).fetchone() is not None:
            bot.send_message(message.chat.id, 'Ваша заявка на регистрацию еще в обработке. '
                                              'Эта команда будет доступна после подтверждения регистрации.')
        else:
            bot.send_message(message.chat.id, 'Эта команда доступна только зарегистрированным пользователям. '
                                              'Чтобы зарегистрироваться, напечатайте "register <player last name>" '
                                              '(/help)')


@bot.message_handler(regexp='^(?i)(today|tomorrow|upcoming)[\s]+[\w]+$')
def send_events_by_query(message):
    period, lastname = (x.lower() for x in message.text.split())
    if cf.is_player(lastname):
        reply = events_list_to_reply(cf.get_events(period, lastname))
        bot.send_message(message.chat.id, reply)


@bot.message_handler(commands=['users'])
@admin_only(bot)
def send_registered_users(message):
    with sqlite3.connect('db.sqlite3') as conn:
        cur = conn.cursor()
        reply = ''
        for record in cur.execute('SELECT * FROM users').fetchall():
            reply += str(record)
    if reply == '':
        reply = 'No users in database.'
    bot.send_message(message.chat.id, reply)


@bot.message_handler(commands=['stats'])
def send_stats(message):
    with open('stats-full.png', 'rb') as image:
        bot.send_photo(message.chat.id, image)


bot.polling()
