import sqlite3
from config import REMIND_TIME, RCOE_CHAT_ID
from calendar_fetcher import get_events, events_list_to_reply


def eleven_not(bot):
    bot.send_message(RCOE_CHAT_ID, '⚠️ Ежедневная напоминалка. Если кто-то сегодня не может играть, '
                                   'то сейчас самое время сообщить об этом! Спасибо!')


def notifier(bot):
    with sqlite3.connect('db.sqlite3') as conn:
        cur = conn.cursor()
        for sub in cur.execute('SELECT * FROM subs').fetchall():
            reply = ''
            for lastname in sub[1].split(' '):
                events_list = get_events('today', lastname)
                if len(events_list) > 0:
                    reply += 'Матчи игрока {} на сегодня:\n'.format(lastname) + \
                             events_list_to_reply(events_list)
            if reply != '':
                bot.send_message(sub[0], reply)
        conn.close()

