# coding: utf-8
from ics import Calendar
from urllib.request import urlopen
from config import ICS_URL, INACTIVE_PLAYERS
from datetime import date, datetime, timedelta
from pyparsing import Word, alphas, nums, Suppress, Optional
import sqlite3, imgkit


def events_list_to_reply(events_list):
    reply = ''
    for event in events_list:
        reply += '{}\n{}\n\n'.format(event.begin.format('DD/MM HH:mm'), event.name)
    if reply == '':
        reply = 'Не запланировано'
    return reply


def get_players_from_event(event):
    template = Optional(Suppress('kicker 2017:') + Word(alphas) + Suppress('/') + Word(alphas) + \
                        Suppress('vs' ^ Word(nums + ':')) + Word(alphas) + Suppress('/') + Word(alphas))
    return template.parseString(event.name.lower()).asList()


def get_score_from_event(event):
    template = Optional(Suppress('kicker 2017:' + Word(alphas) + '/' + Word(alphas)) + ('vs' ^ Word(nums + ':')) +
                        Suppress(Word(alphas) + '/' + Word(alphas)))
    result = template.parseString(event.name.lower())[0]
    result = result.replace('vs', '0:0')
    return result


def filter_by_person(events_list, lastname=None):
    if lastname is not None:
        return list(filter(lambda event: lastname.lower() in get_players_from_event(event), events_list))
    else:
        return events_list


def filter_by_period(events_list, period=None):
    if period == 'today':
        return list(filter(lambda x: x.begin.date() == date.today(), events_list))
    elif period == 'tomorrow':
        return list(filter(lambda x: x.begin.date() == date.today() + timedelta(days=1), events_list))
    elif period == 'upcoming':
        return list(filter(lambda x: x.begin.datetime.replace(tzinfo=None) > datetime.now(), events_list))
    else:
        return events_list


def get_events(period=None, person=None):
    events_list = Calendar(urlopen(ICS_URL).read().decode('iso-8859-1')).events
    for event in events_list:
        event.name = event.name.replace('Â\xa0', ' ')
        event.end = event.end.replace(hours=3)
        event.begin = event.begin.replace(hours=3)
    events_list = filter_by_period(events_list, period)
    events_list = filter_by_person(events_list, person)
    return events_list


def get_all_players():
    players = set()
    for event in get_events():
        for lastname in get_players_from_event(event):
            players.add(lastname)
    players_list = list(players)
    players_list.sort()
    for player in INACTIVE_PLAYERS:
        if player in players_list:
            players_list.remove(player)
    return players_list


def is_player(lastname):
    if lastname in get_all_players():
        return True
    else:
        return False


def events_to_db(event_list):
    modified = False
    with sqlite3.connect('db.sqlite3') as conn:
        cur = conn.cursor()
        for event in event_list:
            players = get_players_from_event(event)
            if len(players) > 0:
                event_date = event.begin.format('DD.MM.YY')
                score = get_score_from_event(event)
                try:
                    cur.execute('INSERT INTO events (timestamp, player0, player1, score, player2, player3, title, "date")'
                                'VALUES (?, ?, ?, ?, ?, ?, ?, ?)', (event.begin.timestamp, players[0], players[1],
                                                                 score, players[2], players[3], event.name, event_date))
                except sqlite3.IntegrityError:
                    modified = True
            conn.commit()
    return modified


# Out: list of tuples
def calculate_stats():
    stats = {}
    with sqlite3.connect('db.sqlite3') as conn:
        cur = conn.cursor()
        event_list = cur.execute('SELECT player0, player1, player2, player3, score FROM events').fetchall()
    for event in event_list:
        player0, player1, player2, player3, score = event
        score = tuple(int(x) for x in score.split(':'))
        score_diff = score[0] - score[1]
        for player in (player0, player1, player2, player3):
            try:
                stats[player]
            except KeyError:
                stats[player] = [0, 0, 0, 0]    # (games, wins, goals_diff, point)
            if score_diff != 0:
                stats[player][0] += 1
        stats[player0][2] += score_diff
        stats[player1][2] += score_diff
        stats[player2][2] -= score_diff
        stats[player3][2] -= score_diff
        if score_diff >= 2:
            for player in (player0, player1):
                stats[player][1] += 1
                stats[player][3] += 3
        elif score_diff == 1:
            for player in (player0, player1):
                stats[player][1] += 1
                stats[player][3] += 2
            for player in (player2, player3):
                stats[player][3] += 1
        elif score_diff == -1:
            for player in (player0, player1):
                stats[player][3] += 1
            for player in (player2, player3):
                stats[player][1] += 1
                stats[player][3] += 2
        elif score_diff <= -2:
            for player in (player2, player3):
                stats[player][1] += 1
                stats[player][3] += 3
    for player in INACTIVE_PLAYERS:
        if player in stats:
            stats.pop(player)
    stats = [(player,) + tuple(stats[player]) for player in stats]
    stats.sort(key=lambda player: player[4], reverse=True)
    return stats


# In: stats from get_stats() as a list of tuples
def stats_to_db(stats):
    with sqlite3.connect('db.sqlite3') as conn:
        cur = conn.cursor()
        cur.execute('DELETE FROM stats')
        for record in stats:
            cur.execute('INSERT INTO stats VALUES (?, ?, ?, ?, ?)', record)
            conn.commit()


def get_stats():
    with sqlite3.connect('db.sqlite3') as conn:
        cur = conn.cursor()
        return cur.execute('SELECT * FROM stats').fetchall()


# In: stats from DB as a list of tuples
def stats2html(stats):
    with open('table.html', 'r') as f:
        reply = f.read()
    data = ''
    for record in stats:
        data += '<tr>'
        for cell in record:
            data += '<td>' + str(cell) + '</td>'
        data += '</tr>'
    reply = reply.replace('{{ data }}', data)
    reply = reply.replace('{{ datetime }}', datetime.now().strftime('%H:%M %d.%m.%y'))
    return reply


def html_to_image(html):
    with open('stats.html', 'w') as f:
        f.write(html)
    imgkit.from_file('stats.html', 'stats-full.png', options={'xvfb': ''})


def stats_to_db_and_image():
    stats = calculate_stats()
    stats_to_db(stats)
    html_to_image(stats2html(stats))