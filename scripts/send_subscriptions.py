from os import chdir
from os.path import dirname, abspath

chdir(dirname(dirname(abspath(__file__))))

from bot import bot
from notifier import notifier

notifier(bot)
