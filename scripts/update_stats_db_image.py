from os import chdir
from os.path import dirname, abspath
from calendar_fetcher import stats_to_db_and_image

chdir(dirname(dirname(abspath(__file__))))

stats_to_db_and_image()
