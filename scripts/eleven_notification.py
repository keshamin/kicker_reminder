from os import chdir
from os.path import dirname, abspath

chdir(dirname(dirname(abspath(__file__))))

from notifier import eleven_not
from bot import bot

eleven_not(bot)
