from os import chdir
from os.path import dirname, abspath
from calendar_fetcher import events_to_db, get_events

chdir(dirname(dirname(abspath(__file__))))

events_to_db(get_events())
